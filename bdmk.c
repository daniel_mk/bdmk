// gcc -g -o0 -o bdmk bdmk.c
// src/bdmk_crud.c src/bdmk_database_creation.c src/bdmk_file_management.c
// src/bdmk_general_utility.c src/bdmk_syntax_validation.c src/bdmk_system_utility.c
// src/bdmk_ugly_data_interpretation.c src/bdmk_ugly_data_structuring.c
// src/bdmk_order.c
// valgrind --leak-check=yes ./bdmk

#include <stdio.h>
#include "src/bdmk_load.h"

#ifdef linux
#define WIN 0
#endif
#ifdef _WIN32
#define WIN 1
#endif

void create_database()
{
    Database * db = &bdmk_struct;

    strcpy((*db).desc, "DBTeste");
    (*db).ntables = 1;

    strcpy((*db).tables[0].desc, "Cliente");
    (*db).tables[0].nfields = 3;
    (*db).tables[0].max_nrows = 1000;
    (*db).tables[0].nrows = 0;

    strcpy((*db).tables[0].fields[0].desc, "id");
    (*db).tables[0].fields[0].len = -10;

    strcpy((*db).tables[0].fields[1].desc, "nome");
    (*db).tables[0].fields[1].len = 32;

    strcpy((*db).tables[0].fields[2].desc, "telefone");
    (*db).tables[0].fields[2].len = 20;

    bdmk_create("BancoTeste");
}

int menu()
{
    int op;

    printf("1 - Recriar banco\n");
    printf("2 - Inserir\n");
    printf("3 - Selecionar\n");
    printf("4 - Atualizar\n");
    printf("5 - Deletar\n");
    printf("0 - Sair\n\n");
    printf("Opcao: ");

    scanf("%d", &op); getchar();
    return op;
}

void inserir()
{
    int id, i;
    char nome[32], telefone[20];
    Query * q;

    printf("Nome: ");
    fgets(nome, 32, stdin);
    nome[strlen(nome)-1] = 0;
    printf("Telefone: ");
    fgets(telefone, 32, stdin);
    telefone[strlen(telefone)-1] = 0;

    bdmk_open("BancoTeste");
    q = bdmk_select("TABLE Cliente; ORDER BY id DESC", id, nome, telefone);
    if((*q).nrows == 0){
        id = 1;
    } else {
        id = bdmk_fetch_int(q, "id") + 1;
    }
    for(i=0; i<10; i++)
        bdmk_insert("TABLE Cliente; id, nome, telefone", id++, nome, telefone);
    bdmk_close();
}

void selecionar()
{
    int id;
    char pnome[32], *nome, *telefone;
    Query *q;

    printf("\n");
    printf("Pesquisar nome: ");
    fgets(pnome, 32, stdin);
    pnome[strlen(pnome)-1] = 0;

    bdmk_open("BancoTeste");
    q = bdmk_select("TABLE Cliente; WHERE nome ~ %s ORDER BY nome OFFSET %d", pnome, 0);
    printf("%d resultados.\n", (*q).totalrows);
    if(!(*q).error){
        for(; (*q).row<(*q).nrows; (*q).row++){
            id = bdmk_fetch_int(q, "id");
            nome = bdmk_fetch_str(q, "nome");
            telefone = bdmk_fetch_str(q, "telefone");
            printf("%d - %s - %s\n", id, nome, telefone);
            free(nome); free(telefone);
        }
    }
    free(q);
    bdmk_close();
}

void atualizar()
{
    int id, n;
    char nome[32], telefone[20];

    printf("Id: ");
    scanf("%d", &id); getchar();
    printf("Nome: ");
    fgets(nome, 32, stdin);
    nome[strlen(nome)-1] = 0;
    printf("Telefone: ");
    fgets(telefone, 32, stdin);
    telefone[strlen(telefone)-1] = 0;

    bdmk_open("BancoTeste");
    n = bdmk_update("TABLE Cliente; nome, telefone WHERE id = %d", nome, telefone, id);
    printf("\nAtualizou %d.\n", n);
    bdmk_close();
}

void deletar()
{
    int id, n;

    printf("Id: ");
    scanf("%d", &id); getchar();

    bdmk_open("BancoTeste");
    n = bdmk_delete("TABLE Cliente; WHERE id = %d", id);
    printf("\nDeletou %d.\n", n);
    bdmk_close();
}

int main()
{
    int op, id, n;

    while(op = menu()){
        switch(op){
            case 1:
                create_database();
            break;
            case 2:
                inserir();
            break;
            case 3:
                selecionar();
            break;
            case 4:
                atualizar();
            break;
            case 5:
                deletar();
            break;
            default:
                printf("\n?\n");
        }
        printf("\nPressione enter para outra opcao...");
        getchar();
        if(WIN) system("cls"); else system("clear");
    }
    return 0;
}

