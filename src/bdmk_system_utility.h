#ifndef BDMK_SYSTEM_UTILITY
#define BDMK_SYSTEM_UTILITY

int _bdmk_get_table_index(char * table_name);
int _bdmk_get_field_index(int table_index, char * field_name);

int _bdmk_is_row_empty(char * str);
int _bdmk_get_first_e_row(int table_index);

void _bdmk_change_nrows(int table_index, int change_by);

#endif
