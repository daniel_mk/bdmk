#include "bdmk_general_utility.h"

int _bdmk_cin_array(char c, char * arr)
{
    int i, len = strlen(arr);
    for(i=0; i<len; i++){
        if(arr[i] == c) return 1;
    }
    return 0;
}

int _bdmk_cin_range(char c, char s, char e)
{
    return c >= s && c <= e;
}

int _bdmk_is_word(char c, int pos)
{
    return _bdmk_cin_range(c, 'a', 'z') || _bdmk_cin_range(c, 'A', 'Z') ||
           (_bdmk_cin_range(c, '0', '9') && pos) || c == '_';
}

char * _bdmk_get_word(int pos, int * final_pos, char * str, int type)
{
    int i = 0;
    char * word = malloc(32);
    while(str[pos] == ' '){ pos++; }
    switch(type){
        case 0:
            while(_bdmk_is_word(str[pos], i)){
                word[i++] = str[pos++];
            }
            break;
        case 1:
            while(_bdmk_cin_array(str[pos], ";&|,=<>~")){
                word[i++] = str[pos++];
            }
            break;
        case 2:
            while(!_bdmk_cin_array(str[pos], ";&|,=<>~ ") && str[pos] != '\0'){
                word[i++] = str[pos++];
            }
    }
    word[i] = 0;
    *final_pos = pos;
    return word;
}

int min(int a, int b)
{
    return a < b ? a : b;
}

int max(int a, int b)
{
    return a > b ? a : b;
}

void _bdmk_lcase(char * str)
{
    int len = strlen(str), i;
    for(i=0; i<len; i++){
        if(_bdmk_cin_range(str[i], 'A', 'Z')){
            str[i] += 'a' - 'A';
        }
    }
}
