#ifndef BDMK_SYNTAX_VALIDATION
#define BDMK_SYNTAX_VALIDATION

char * _bdmk_get_table_filter(int pos, int * final_pos, char * query, char * last_str);
char ** _bdmk_insert_filter(char * query);
char ** _bdmk_get_filter(char * query);
char ** _bdmk_delete_filter(char * query);

#endif
