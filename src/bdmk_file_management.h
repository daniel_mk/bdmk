#ifndef BDMK_FILE_MANAGEMENT
#define BDMK_FILE_MANAGEMENT
#include <stdio.h>

extern FILE * bdmk_file;

void _bdmk_set_pos(int pos);
int _bdmk_get_pos();

int _bdmk_write_str(char * str, int len);
int _bdmk_write_int(int num);

char * _bdmk_read(int len);
char * _bdmk_read_row(int len);

#endif
