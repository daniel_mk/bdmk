#ifndef BDMK_DATABASE_CREATION
#define BDMK_DATABASE_CREATION
#include <stdio.h>

extern FILE * bdmk_file;

int _bdmk_set_table_info(int table_index, int last_pos);
void bdmk_create(char * file_name);

#endif
