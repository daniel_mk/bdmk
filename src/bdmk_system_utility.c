#include "bdmk_struct.h"
#include "bdmk_system_utility.h"

int _bdmk_get_table_index(char * table_name)
{
    int i;
    for(i=0; i<bdmk_struct.ntables; i++){
        if(!strcmp(table_name, bdmk_struct.tables[i].desc)) return i;
    }
    return -1;
}

int _bdmk_get_field_index(int table_index, char * field_name)
{
    int i;
    Table * t = &bdmk_struct.tables[table_index];
    for(i=0; i<(*t).nfields; i++){
        if(!strcmp(field_name, (*t).fields[i].desc)) return i;
    }
    return -1;
}

int _bdmk_is_row_empty(char * str)
{
    return str[0] == 3;
}

int _bdmk_get_first_e_row(int table_index)
{
    int i, found; char * str;
    Table * t = &bdmk_struct.tables[table_index];

    if((*t).nrows == (*t).max_nrows) return -1;

    _bdmk_set_pos((*t).data_pos);
    for(found=i=0; i<(*t).max_nrows && !found; i++){
        str = _bdmk_read_row((*t).row_len);
        if(_bdmk_is_row_empty(str)) found = 1;
        free(str);
    }
    return found ? _bdmk_get_pos() - (*t).row_len : -1;
}

void _bdmk_change_nrows(int table_index, int change_by)
{
    int nrows_pos;
    Table * t = &bdmk_struct.tables[table_index];

    (*t).nrows += change_by;

    nrows_pos = (*t).data_pos - (*t).nfields * 42 - 20;
    _bdmk_set_pos(nrows_pos);
    _bdmk_write_int((*t).nrows);
}

