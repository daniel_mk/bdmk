#ifndef BDMK_UGLY_DATA_INTERPRETATION
#define BDMK_UGLY_DATA_INTERPRETATION
#include <stdarg.h>

int _bdmk_get_int_row(int table_index, char * row, int index);
int _bdmk_cmp_int(int ti, char * row, int * values, int a, int op, int b);
char * _bdmk_get_str_row(int table_index, char * row, int index);
int _bdmk_cmp_str(int ti, char * row, char ** values, int a, int op, int b);

int * _bdmk_where(int table_index, char * table_name, char * query, va_list * valist);

#endif
