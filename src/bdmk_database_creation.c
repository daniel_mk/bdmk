#include "bdmk_struct.h"
#include "bdmk_database_creation.h"

int _bdmk_set_table_info(int table_index, int last_pos)
{
    int row_len, i;
    Table * t = &bdmk_struct.tables[table_index];

    for(row_len=i=0; i<(*t).nfields; i++){
        (*t).fields[i].pos = row_len;
        row_len += abs((*t).fields[i].len);
    }
    (*t).row_len = row_len;
    (*t).data_pos = last_pos + 62 + (*t).nfields * 42;
    return (*t).data_pos + (*t).max_nrows * row_len;
}

void bdmk_create(char * file_name)
{
    int i, j, tab = 42;
    char full_file_name[36];
    Database *db = &bdmk_struct;

    strcpy(full_file_name, file_name);
    strcat(full_file_name, ".txt");

    bdmk_file = fopen(full_file_name, "w+b");

    _bdmk_write_str((*db).desc, 32);
    _bdmk_write_int((*db).ntables);
    for(i=0; i<(*db).ntables; i++){
        _bdmk_write_str((*db).tables[i].desc, 32);
        _bdmk_write_int((*db).tables[i].max_nrows);
        _bdmk_write_int((*db).tables[i].nrows);
        _bdmk_write_int((*db).tables[i].nfields);
        for(j=0; j<(*db).tables[i].nfields; j++){
            _bdmk_write_str((*db).tables[i].fields[j].desc, 32);
            _bdmk_write_int((*db).tables[i].fields[j].len);
        }
        tab = _bdmk_set_table_info(i, tab);
        for(j=0; j<(*db).tables[i].max_nrows; j++){
            _bdmk_write_str("", (*db).tables[i].row_len);
        }
    }
    fclose(bdmk_file);
}
