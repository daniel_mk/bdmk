#ifndef BDMK_STRUCT
#define BDMK_STRUCT

typedef struct{
    int len, pos;
    char desc[32];
} Field;

typedef struct {
    int nfields, max_nrows, nrows;
    char desc[32];
    Field fields[16];
    int data_pos, row_len;
} Table;

typedef struct {
    int ntables;
    char desc[32];
    Table tables[8];
} Database;

Database bdmk_struct;

typedef struct {
    int ncmp, nparam, err_no;
    char param_type[16];
    int cmp_desc[16][4];
} Query_Map;

typedef struct {
    int table_index;
    int row, nrows, totalrows, error;
    int row_desc[50];
} Query;

#endif
