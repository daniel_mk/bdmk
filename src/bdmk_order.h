#ifndef BDMK_ORDER
#define BDMK_ORDER

int _bdmk_compare_quick(int tbl_i, int * v, int * fields, int n, char type, int * int_val, char ** str_val);
int _bdmk_in_range_order(int start, int end, int r_start, int r_end);
void _bdmk_quick(int tbl_i, int * v, int start, int end, int r_start, int r_end, int * fields, int asc);

int _bdmk_order(int table_index, char * query, int * rows, int offset, int desc);

#endif
