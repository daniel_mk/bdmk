#include "bdmk_ugly_data_structuring.h"

int _bdmk_param_type(char * word)
{
    if(strlen(word) != 2) return 0;
    return _bdmk_cin_array(word[1], "cds") ? word[1] : 0;
}

int _bdmk_sub_make_query_map(int * pos, char * query, int index, int table_index, Query_Map ** map, char ** error)
{
    char * aux; int ptype, ind;
    aux = _bdmk_get_word(*pos, pos, query, 2);
    if(aux[0] == '%'){
        if(!(ptype = _bdmk_param_type(aux))){
            *error = aux;
            return -9999; // par�metro inv�lido
        }
        (**map).cmp_desc[(**map).ncmp][index] = -1 * (**map).nparam - 1;
        (**map).param_type[(**map).nparam++] = ptype;
    } else {
        if((ind = _bdmk_get_field_index(table_index, aux)) == -1){
            *error = aux;
            return -9998; // campo inv�lido
        }
        (**map).cmp_desc[(**map).ncmp][index] = ind;
    }
    return 1;
}

Query_Map * _bdmk_make_query_map(int table_index, char * query, char ** error)
{
    int pos, ret, len;
    Query_Map * map = malloc(sizeof(Query_Map));
    char * aux, ptype;
    (*map).err_no = (*map).ncmp = (*map).nparam = pos = 0;
    len = strlen(query);
    do{
        if((ret = _bdmk_sub_make_query_map(&pos, query, 0, table_index, &map, error)) != 1){
            (*map).err_no = ret;
            return map;
        }

        aux = _bdmk_get_word(pos, &pos, query, 1);
        if(strcmp(aux, "=") && strcmp(aux, "~") && strcmp(aux, ">") && strcmp(aux, "<") &&
           strcmp(aux, ">=") && strcmp(aux, "<=") && strcmp(aux, "><")){
            error = aux;
            (*map).err_no = -9997; // Operador inv�lido
            return map;
        }
        (*map).cmp_desc[(*map).ncmp][1] = aux[0] + aux[1];

        if((ret = _bdmk_sub_make_query_map(&pos, query, 2, table_index, &map, error)) != 1){
            (*map).err_no = ret;
            return map;
        }

        if((*map).cmp_desc[(*map).ncmp][0] < 0 && (*map).cmp_desc[(*map).ncmp][2] < 0){
            (*map).err_no = -9996; // Nenhum campo verificado
            return map;
        }

        int param, field_index, a, b;

        a = (*map).cmp_desc[(*map).ncmp][0];
        b = (*map).cmp_desc[(*map).ncmp][2];
        if(a < 0 || b < 0){
            param = min(a, b);
            field_index = max(a, b);
            if(((*map).param_type[abs(param) - 1] == 'd' &&
               bdmk_struct.tables[table_index].fields[field_index].len != -10) ||
               ((*map).param_type[abs(param) - 1] == 'c' &&
               bdmk_struct.tables[table_index].fields[field_index].len != 1) ||
               ((*map).param_type[abs(param) - 1] == 's' &&
               bdmk_struct.tables[table_index].fields[field_index].len == -10)){
                *error = &bdmk_struct.tables[table_index].fields[field_index].desc;
                (*map).err_no = -9995; // Campo com par�metro errado
                return map;
            }
        }
        if(pos < len){
            aux = _bdmk_get_word(pos, &pos, query, 1);
            if(strcmp(aux, "|") && strcmp(aux, "&")){
                *error = aux;
                (*map).err_no = -9997; // Operador inv�lido
                return map;
            }
            (*map).cmp_desc[(*map).ncmp][3] = aux[0];
        } else {
            (*map).cmp_desc[(*map).ncmp][3] = 0;
        }
        (*map).ncmp++;
    } while(pos < len);

    return map;
}
